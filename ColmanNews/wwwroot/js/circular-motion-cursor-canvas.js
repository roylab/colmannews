﻿const canvas = document.querySelector("canvas.background");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const context = canvas.getContext("2d");

let mouse = { x: null, y: null };
window.addEventListener("mousemove", ({ x, y }) => (mouse = { x, y }));

window.addEventListener("resize", () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    init();
});

const gravity = 0.1;
const friction = 0.9;

const Particle = class {
    constructor(x, y, radius, color, dx = 0, dy = 0) {
        this.x = x;
        this.origX = x;
        this.y = y;
        this.origY = y;
        this.radius = radius;
        this.color = color;
        this.radians = Math.random() * Math.PI * 2;
        this.velocity = 0.05;
        this.distanceFromCenter = Math.random() * 70 + 50;
        this.lastPoint = { x, y };
        this.lastMouse = { x, y };
    }

    draw() {
        const { x, y, radius, lastPoint, color } = this;
        context.beginPath();
        context.strokeStyle = color;
        context.lineWidth = radius;
        context.moveTo(lastPoint.x, lastPoint.y);
        context.lineTo(x, y);
        context.stroke();
        context.closePath();

        return this;
    }

    update() {
        this.lastMouse.x += (mouse.x - this.lastMouse.x) * 0.05;
        this.lastMouse.y += (mouse.y - this.lastMouse.y) * 0.05;
        this.origX = mouse.x;
        this.origY = mouse.y;
        this.lastPoint = { x: this.x, y: this.y };
        this.radians += this.velocity;
        this.x =
            this.lastMouse.x + Math.cos(this.radians) * this.distanceFromCenter;
        this.y =
            this.lastMouse.y + Math.sin(this.radians) * this.distanceFromCenter;

        return this;
    }
};

let sun = null;
let planet = null;

const animate = () => {
    requestAnimationFrame(animate);
    canvas.style.backgroundColor = "rgba(0,0,0, 1)";

    context.fillStyle = "rgba(255,255,255,0.05)";
    context.fillRect(0, 0, canvas.width, canvas.height);
    particles.forEach((p) => p.update().draw());
};

let particles = null;
const colors = ["#58b7b8", "#f8ce3d", "#df6127", "#f2e4b1", "#12403e"];

const init = () => {
    particles = [];

    for (let i = 0; i < 50; i++) {
        particles.push(
            new Particle(
                mouse.x,
                mouse.y,
                Math.random() + 1,
                colors[Math.floor(Math.random() * colors.length)]
            )
        );
    }
};

window.addEventListener("click", init);

init();
animate();
