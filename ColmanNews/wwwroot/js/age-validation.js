﻿const validateAge = () =>
    $("#min-age, #max-age").change(
        (event) => {
            const minAge = $("#min-age")[0].value;
            const maxAge = $("#max-age")[0].value;

            let errorMessage = "";
            let disabled = false;

            if (maxAge && minAge && parseInt(maxAge) <= parseInt(minAge)) {
                errorMessage = "Invalid age range";
                disabled = true;
            } 

            $("#search-smurfs").prop("disabled", disabled);
            $("#search-smurf-error-message")[0].textContent = errorMessage;
        }
    );

$(() => {
    validateAge();
})

