﻿const removeIncorrectCredentialsMessageWhenUnnecessary = () => 
    $(".login-password-input, .login-username-input").on(
        "keyup",
        () => {
            $(".incorrect-credentials-message")[0].textContent = "";
        })

$(() => {
    removeIncorrectCredentialsMessageWhenUnnecessary()
})

