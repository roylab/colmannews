﻿const addUsernameAvailabilityCheck = () =>
    $(".input-register-username").on(
    "keyup",
    (event) => {
        $.ajax({
            method: "POST",
            url: "/Users/IsUsernameAvailable",
            data: { username: event.target.value }
        }).done(data => {
            $(".username-unavailable-message")[0].textContent =
                !data.isUsernameAvailable ?
                    "This username is already taken." :
                    "";
        })
    }
    );

$(() => {
    addUsernameAvailabilityCheck();
})

