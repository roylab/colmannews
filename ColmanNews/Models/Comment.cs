﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerce.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false), Required]
        [DisplayName("Date")]
        public DateTime CreationDate { get; set; }
       
        public string CommenterUserName { get; set; }

        [Required]
        public User Commenter { get; set; }

        [DisplayName("Article")]
        public int ArticleId { get; set; }

        [Required]
        public Article Article { get; set; }

    }
}
