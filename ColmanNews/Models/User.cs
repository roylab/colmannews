﻿using ColmanNews.Models.utils;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace eCommerce.Models
{

    public enum Role
    {
        Admin, Reporter, Reader
    }
    public class User
    {
        [Key]
        [Required]
        [StringLength(20)]
        [RegularExpression("([a-zA-Z0-9]+)", ErrorMessage = "Enter only letters and numbers for username")]
        public string Username { get; set; }

        [Required]
        [StringLength(20)]
        [RegularExpression("([a-zA-Z0-9]+)", ErrorMessage = "Enter only letters and numbers for password")]
        public string Password { get; set; }

        [Required]
        [DefaultValue(Role.Reader)]
        [Range(0,2)]
        [DisplayName("User Role")]
        public Role UserRole { get; set; }

        [Required]
        [StringLength(20)]
        [RegularExpression("([a-zA-Z]+[a-zA-Z- ]*[a-zA-Z]+)|([a-zA-Z]){1}", ErrorMessage = "Invalid Name")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [StringLength(20)]
        [RegularExpression("([a-zA-Z]+[a-zA-Z- ]*[a-zA-Z]+)|([a-zA-Z]){1}", ErrorMessage = "Invalid Name")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        [DisplayName("Birth Date")]
        [DataType(DataType.Date)]
        // Custom validation attribute
        [MinimumAge(10, ErrorMessage = "You must be a least 10 years old")]
        public DateTime BirthDate { get; set; }

        public string Address { get; set; }
    }
}
