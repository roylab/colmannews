﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace eCommerce.Models
{
    public enum CounterRaiser
    {
        ArticleVisit, CommentPost
    }

    public class UserGenre
    {
        public string Username { get; set; }

        public User User { get; set; }

        public int GenreId { get; set; }

        public Genre Genre { get; set; }

        [Required]
        [DefaultValue(0)]
        public int Counter { get; set; }

        public void RaiseCounter(CounterRaiser counterRaiser)
        {
            switch(counterRaiser)
            {
                case CounterRaiser.ArticleVisit:
                {
                    Counter++;
                    break;
                }
                case CounterRaiser.CommentPost:
                {
                    Counter = Counter + 2;
                    break;
                 }
                default:
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}