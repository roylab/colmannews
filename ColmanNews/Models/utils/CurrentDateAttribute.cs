﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ColmanNews.Models.utils
{
    public class MinimumAgeAttribute : ValidationAttribute
    {
        private int ageInYears;

        public MinimumAgeAttribute(int ageInYears)
        {
            this.ageInYears = ageInYears;
        }

        public override bool IsValid(object value)
        {
            var dt = (DateTime)value;
            DateTime earliestValidBirthdate = (DateTime.Now).AddYears(-ageInYears);
                
            if (dt <= earliestValidBirthdate)
            {
                return true;
            }
            return false;
        }
    }
}
