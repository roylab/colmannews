﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColmanNews.Data;
using eCommerce.Models;
using Newtonsoft.Json;

namespace ColmanNews.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ColmanNewsContext _context;

        public CommentsController(ColmanNewsContext context)
        {
            _context = context;
        }

        // GET: Comments
        public async Task<IActionResult> Index()
        {
            var colmanNewsContext = _context.Comments.Include(c => c.Article).Include(c => c.Commenter);
            return View(await colmanNewsContext.ToListAsync());
        }

        // GET: Comments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comments
                .Include(c => c.Article)
                .Include(c => c.Commenter)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // GET: Comments/Create
        public IActionResult Create()
        {
            ViewData["ArticleId"] = new SelectList(_context.Articles, "Id", "Content");
            ViewData["CommenterUserName"] = new SelectList(_context.Users, "Username", "Username");
            return View();
        }

        // POST: Comments/Create
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Comment comment)
        {
            comment.Commenter = await _context.Users.FindAsync(comment.CommenterUserName);
            comment.CreationDate = DateTime.Now;
            comment.Article = await _context.Articles.FindAsync(comment.ArticleId);

            if (comment.Commenter != null && comment.Article != null)
            {
                try
                {
                    _context.Add(comment);
                    await UpdateUserGenreCount(comment, false);
                    await _context.SaveChangesAsync();
                    return Json(new { success = true, responseText = JsonConvert.SerializeObject(comment) });
                } catch(Exception e)
                {
                    return Json(new { success = false, responseText = "Something failed" });
                }
            }
            return BadRequest();
        }

        // GET: Comments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comments.FindAsync(id);
            if (comment == null)
            {
                return NotFound();
            }
            ViewData["ArticleId"] = new SelectList(_context.Articles, "Id", "Content", comment.ArticleId);
            ViewData["CommenterUserName"] = new SelectList(_context.Users, "Username", "Username", comment.CommenterUserName);
            return View(comment);
        }
        
        // POST: Comments/Edit
        [HttpPost]
        public async Task<IActionResult> Edit([FromBody] Comment newComment)
        {
            var comment = await _context.Comments.FindAsync(newComment.Id);

            if (comment != null)
            {
                comment.Content = newComment.Content;

                try
                {
                    _context.Update(comment);
                    await _context.SaveChangesAsync();
                    return Json(new { success = true, responseText = JsonConvert.SerializeObject(comment) });
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Json(new { success = false, responseText = "Something failed" });
                }
            }

            return BadRequest();
        }

        // GET: Comments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comments
                .Include(c => c.Article)
                .Include(c => c.Commenter)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed([FromBody] Comment comment)
        {
            var commentToDelete = await _context.Comments.FindAsync(comment.Id);
            _context.Comments.Remove(commentToDelete);
            await _context.SaveChangesAsync();
            return Json(new { success = true, responseText = JsonConvert.SerializeObject(commentToDelete) });
        }

        private bool CommentExists(int id)
        {
            return _context.Comments.Any(e => e.Id == id);
        }

        private async Task UpdateUserGenreCount(Comment comment, bool saveChanges = true)
        {
            var username = User.Claims.FirstOrDefault(c => c.Type == "Username")?.Value;
            var userGenre = await _context.UserGenres.FirstOrDefaultAsync(userGenre => userGenre.Username == username && userGenre.GenreId == comment.Article.GenreId);
            userGenre.RaiseCounter(CounterRaiser.CommentPost);
            _context.Update(userGenre);
            if (saveChanges)
            {
                await _context.SaveChangesAsync();
            }
        }
    }
}
