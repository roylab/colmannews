﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ColmanNews.Data;
using eCommerce.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.CodeAnalysis.FlowAnalysis;
using Microsoft.EntityFrameworkCore;

namespace ColmanNews.Controllers
{
    public class UsersController : Controller
    {
        private readonly ColmanNewsContext _context;

        public UsersController(ColmanNewsContext context)
        {
            _context = context;
        }

        public IActionResult Login()
        {
            // authenticated users cannot login
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Home", "Articles");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(string Username, string Password)
        {
            var users = _context.Users.Where(user => user.Username == Username && user.Password == Password).ToList();
            ViewData["FailedLogin"] = false;

            if (users != null && users.Count() > 0)
            {
                await RegisterCookieForUser(users.First());
                return RedirectToAction("Home", "Articles");
            }

            ViewData["FailedLogin"] = true;

            return View();
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction(nameof(Login));
        }

        private async Task RegisterCookieForUser(User user)
        {
            var claims = new List<Claim>
            {
                new Claim("Username", user.Username),
                new Claim("FirstName", user.FirstName),
                new Claim(ClaimTypes.Role, user.UserRole.ToString())
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var authProperties = new AuthenticationProperties();

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);

        }

        [HttpPost]
        public ActionResult IsUsernameAvailable(string username)
        {
            bool isUsernameAvailable = _context.Users.Where(user => user.Username == username).Count() == 0;

            return Json(new { isUsernameAvailable });
        }

        public IActionResult Register()
        {
            // authenticated users cannot register
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Home", "Genres");
            }

            return View();
        }

        public async Task<IActionResult> Map()
        {
            return View(_context.Users.Where(user => user.UserRole == Role.Reporter).ToList());
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Username,Password,FirstName,LastName,BirthDate")] User user)
        {
            if (ModelState.IsValid)
            {
                user.UserRole = Role.Reader;
                _context.Add(user);
                await _context.SaveChangesAsync();
                RegisterCookieForUser(user);


                return RedirectToAction("Home", "Genres");
            }

            return View(nameof(Register));
        }

        private bool IsNull(object obj)
        {
            return obj == null;
        }

        private static int CalculateAge(DateTime dateOfBirth)
        {
            int age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }

        [HttpGet("Reporters")]

        public async Task<ActionResult> GetReporters()
        {
            var reporters = await _context.Users.Where(user => user.UserRole == Role.Reporter).ToListAsync();
            return Json(reporters);
        }

        //GET: Users/SearchResults
        public async Task<IActionResult> SearchResults(
            string firstName,
            string lastName,
            string minAge,
            string maxAge,
            string role)
        {

            bool firstNameExists = !IsNull(firstName);
            bool lastNameExists = !IsNull(lastName);
            bool minAgeExists = !IsNull(minAge);
            bool maxAgeExists = !IsNull(maxAge);
            bool roleExists = !IsNull(role);

            var matchingUsersByName = await _context.Users
                       .Where(user =>
                                   (firstNameExists ? user.FirstName.ToLower().Contains(firstName.ToLower()) : true)
                                && (lastNameExists ? user.LastName.ToLower().Contains(lastName.ToLower()) : true)
                       ).ToListAsync();

            List<User> matchingUsers = new List<User>(matchingUsersByName);

            foreach (User user in matchingUsersByName)
            {
                bool matchesMinAge = minAgeExists ? CalculateAge(user.BirthDate) >= Convert.ToInt32(minAge) : true;
                bool matchesMaxAge = maxAgeExists ? CalculateAge(user.BirthDate) <= Convert.ToInt32(maxAge) : true;
                bool matchesRole = roleExists ? user.UserRole.ToString().Equals(role) : true;

                if (!matchesMinAge || !matchesMaxAge || !matchesRole)
                {
                    matchingUsers.Remove(user);
                }
            }

            return View(matchingUsers);
        }
    }
}
