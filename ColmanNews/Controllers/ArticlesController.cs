﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ColmanNews.Data;
using eCommerce.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;

namespace ColmanNews.Controllers
{
    public class ArticlesController : Controller
    {
        private readonly ColmanNewsContext _context;

        public ArticlesController(ColmanNewsContext context)
        {
            _context = context;
        }

        // GET: Articles
        public async Task<IActionResult> Index()
        {
            return View(await GetReporterArticles());
        }

        // GET: Articles
        public async Task<IActionResult> Home()
        {
            ViewBag.Users = await _context.Users.ToListAsync();
            ViewBag.Genres = await _context.Genres.ToListAsync();
            return View(await GetArticles());
        }

        // GET: Articles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var article = await _context.Articles
                .Include(article => article.Author)
                .Include(article => article.Genre)
                .Include(article => article.Image)
                .Include(article => article.Comments)
                    .ThenInclude(comment => comment.Commenter)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (article == null)
            {
                return NotFound();
            }

            await UpdateUserGenreCount(article);
            ViewBag.SuggestedArticles = await GetSuggestedArticles(article);
            return View(article);
        }

        // GET: Articles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Articles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Content,GenreId,AuthorUserName")] Article article)
        {
            article.Author = await _context.Users.FindAsync(this.User.Claims.FirstOrDefault(c => c.Type == "Username")?.Value);
            article.CreationDate = DateTime.Now;

            var genre = await _context.Genres.FindAsync(article.GenreId);
            article.Genre = genre;

            if (article.Author != null)
            {
                Image img = new Image();
                if (Request.Form.Files.Count() > 0)
                {
                    var file = Request.Form.Files[0];
                    img.ImageTitle = file.FileName;

                    MemoryStream ms = new MemoryStream();
                    file.CopyTo(ms);
                    img.ImageData = ms.ToArray();

                    ms.Close();
                    ms.Dispose();

                    _context.Add(img);
                    _context.SaveChanges();

                    article.ImageId = img.Id;
                }
                _context.Add(article);
                await _context.SaveChangesAsync();

                var numberOfArticlesPostedTodayInSameGenre = _context.Articles
                    .Where(a => a.GenreId == article.GenreId).AsEnumerable()
                    .Where(a => IsSameDate(a.CreationDate, DateTime.Now)).Count();

                // Post to Facebook if it's the first post of the day in it's genre
                if (numberOfArticlesPostedTodayInSameGenre == 1)
                {
                    using (var client = new HttpClient())
                    {
                        const string KEY = "EAAFyqHgDOFQBAIJeBX8U2FksPHCF3i53QYnccXmunApGZBZANbZB9uceoViHdSPb6mBFxU79Bm28lZCMbiAdXOAv07OKPRQjv3DkZCgg1xTrHDCIjBo3fFHVLOccr4p4UL7liLIEh0TomT5tq0nkhbHNG9kUqCGHviqnjJLMfoCEsTWDrVzbYkHlZAN7IVtGIZD";
                        string post = "📰👓 Check out our new article in " + article.Genre.Name + "! ➡ " + article.Title;

                        HttpRequestMessage requestMessage = new HttpRequestMessage(
                            HttpMethod.Post,
                            "https://graph.facebook.com/110024000913635/feed?message=" + post + "&access_token=" + KEY
                            );

                        HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
                    }
                };

                return RedirectToAction(nameof(Index));
            }
            return View(article);
        }

        private bool IsSameDate(DateTime firstDate, DateTime secondDate)
        {
            return firstDate.Date == secondDate.Date
                && firstDate.Month == secondDate.Month
                && firstDate.Year == secondDate.Year;
        }

        // GET: Articles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var article = await _context.Articles.Include(article => article.Genre).Where(article => article.Id == id).FirstOrDefaultAsync();
            if (article == null)
            {
                return NotFound();
            }
            return View(article);
        }

        // POST: Articles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Content,GenreId,AuthorUserName")] Article article)
        {
            if (id != article.Id)
            {
                return NotFound();
            }
            article.Author = await _context.Users.FindAsync(this.User.Claims.FirstOrDefault(c => c.Type == "Username")?.Value);

            if (article.Author != null)
            {
                Image img = new Image();
                if (Request.Form.Files.Count() > 0)
                {
                    var file = Request.Form.Files[0];
                    img.ImageTitle = file.FileName;

                    MemoryStream ms = new MemoryStream();
                    file.CopyTo(ms);
                    img.ImageData = ms.ToArray();

                    ms.Close();
                    ms.Dispose();

                    _context.Add(img);
                    _context.SaveChanges();

                    article.ImageId = img.Id;
                } else
                {
                    Article savedInDbArticle = await _context.Articles
                            .Include(article => article.Author)
                            .Include(article => article.Genre)
                            .Include(article => article.Image)
                            .Include(article => article.Comments)
                            .ThenInclude(comment => comment.Commenter)
                            .FirstOrDefaultAsync(m => m.Id == id);
                    article.ImageId = savedInDbArticle.ImageId;
                    article.Image = savedInDbArticle.Image;

                    _context.Entry(savedInDbArticle).State = EntityState.Detached;
                }

                var genre = await _context.Genres.FindAsync(article.GenreId);
                article.Genre = genre;
                try
                {
                    _context.Update(article);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArticleExists(article.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(article);
        }

        // GET: Articles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var article = await _context.Articles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (article == null)
            {
                return NotFound();
            }

            return View(article);
        }

        // POST: Articles/Delete
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed([FromBody] Article article)
        {
            var articleToDelete = await _context.Articles.FindAsync(article.Id);
            if (articleToDelete != null)
            {
                _context.Articles.Remove(articleToDelete);
                await _context.SaveChangesAsync();
                return Json(new { success = true, responseText = JsonConvert.SerializeObject(articleToDelete) });
            }

            _context.Articles.Remove(articleToDelete);
            await _context.SaveChangesAsync();
            return Json(new { success = false, responseText = JsonConvert.SerializeObject(articleToDelete) });
        }

        private bool ArticleExists(int id)
        {
            return _context.Articles.Any(e => e.Id == id);
        }

        private async Task<ICollection<Article>> GetReporterArticles()
        {
            var username = this.User.Claims.FirstOrDefault(c => c.Type == "Username")?.Value;
            var myArticles = await _context.Articles
                .Include(article => article.Genre)
                .Include(article => article.Comments)
                .Include(article => article.Image)
                .Include(article => article.Author)
                .Where(article => article.AuthorUserName == username)
                .OrderByDescending(article => article.CreationDate)
                .ToListAsync();
            return myArticles;
        }

        private async Task<ICollection<Article>> GetArticles()
        {
            var latestArticles = await _context.Articles
                .Include(article => article.Author)
                .Include(article => article.Genre)
                .Include(article => article.Comments)
                .OrderByDescending(article => article.CreationDate)
                .ToListAsync();
            ViewBag.articleImageUrls = new Dictionary<int, string>();
            foreach (var article in latestArticles)
            {
                if (article.ImageId != null) {
                    Image img = _context.Image.SingleOrDefault(m => m.Id == article.ImageId);
                    string imageBase64Data = Convert.ToBase64String(img.ImageData);
                    string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);

                    ViewBag.articleImageUrls.Add(article.Id, imageDataURL);
                }
            }
            return latestArticles;
        }

        private async Task UpdateUserGenreCount(Article article)
        {
            var username = User.Claims.FirstOrDefault(c => c.Type == "Username")?.Value;
            var userGenre = await _context.UserGenres.FirstOrDefaultAsync(userGenre => userGenre.Username == username && userGenre.GenreId == article.GenreId);
            userGenre.RaiseCounter(CounterRaiser.ArticleVisit);
            _context.Update(userGenre);
            await _context.SaveChangesAsync();
        }

        private async Task<ICollection<Article>> GetSuggestedArticles(Article article)
        {
            var username = User.Claims.FirstOrDefault(c => c.Type == "Username")?.Value;
            var favoriteUserGenre = await _context.UserGenres
                .Where(userGenre => userGenre.Username == username)
                .OrderByDescending(userGenre => userGenre.Counter)
                .FirstAsync();

            var suggestedArticles = await _context.Articles
                .Include(article => article.Author)
                .Include(article => article.Image)
                .Where(art => art.GenreId == favoriteUserGenre.GenreId && art.Id != article.Id)
                .OrderByDescending(art => art.CreationDate)
                .Take(3)
                .Include(art => art.Genre)
                .ToListAsync();

            return suggestedArticles;
        }


        private bool IsNull(object obj)
        {
            return obj == null;
        }

        public IActionResult OnGetPartial() =>
            new PartialViewResult
            {
                ViewName = "_Search",
                ViewData = ViewData,
            };

        // GET: Articles/SearchResults
        public async Task<IActionResult> SearchResults(string searchTerm, string reporterUsername, string genreId)
        {
            var searchTermExists = !IsNull(searchTerm);
            var reporterUsernameExists = !IsNull(reporterUsername);
            var genreIdExists = !IsNull(genreId);

            var matchingArticles = await _context.Articles
                   .Where(article =>
                        (searchTermExists ? article.Title.ToLower().Contains(searchTerm.ToLower()) : true)
                   && (reporterUsernameExists ? article.AuthorUserName.Equals(reporterUsername) : true)
                   && (genreIdExists ? article.GenreId.ToString().Equals(genreId) : true)
                   )
                   .ToListAsync();

            var reporter = _context.Users.Where(user => user.Username == reporterUsername).FirstOrDefault();

            ViewBag.SearchTerm = searchTerm;
            ViewBag.ReporterName = reporter != null ? reporter.FirstName + " " + reporter.LastName : null;
            ViewBag.Genre = genreIdExists ? _context.Genres.Where(genre => genre.Id.ToString() == genreId).First().Name : null;

            var latestArticles = await _context.Articles
                .Include(article => article.Author)
                .Include(article => article.Genre)
                .Include(article => article.Comments)
                .OrderByDescending(article => article.CreationDate)
                .ToListAsync();
            ViewBag.articleImageUrls = new Dictionary<int, string>();
            foreach (var article in latestArticles)
            {
                if (article.ImageId != null)
                {
                    Image img = _context.Image.SingleOrDefault(m => m.Id == article.ImageId);
                    string imageBase64Data = Convert.ToBase64String(img.ImageData);
                    string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);

                    ViewBag.articleImageUrls.Add(article.Id, imageDataURL);
                }
            }

            return View(matchingArticles);
        }

        [HttpGet("MostCommented")]
        public async Task<ActionResult> GetMostCommented()
        {
            var articles =  _context.Articles.OrderByDescending(u => u.Comments.Count); 
            return Json(articles);
        }

        [HttpGet("MostRecent")]
        public async Task<ActionResult> GetMostRecent()
        {
            var articles = _context.Articles.OrderByDescending(u => u.CreationDate);
            return Json(articles);
        }
    }
}
