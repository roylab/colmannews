﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColmanNews.Data;
using eCommerce.Models;
using System.IO;

namespace ColmanNews.Controllers
{
    public class ImagesController : Controller
    {
        private readonly ColmanNewsContext _context;

        public ImagesController(ColmanNewsContext context)
        {
            _context = context;
        }

        [HttpPost]
        public void UploadImage()
        {
            foreach (var file in Request.Form.Files)
            {
                Image img = new Image();
                img.ImageTitle = file.FileName;

                MemoryStream ms = new MemoryStream();
                file.CopyTo(ms);
                img.ImageData = ms.ToArray();

                ms.Close();
                ms.Dispose();

                _context.Add(img);
                _context.SaveChanges();
            }
        }

        [HttpGet("RetrieveImage")]
        public ActionResult RetrieveImage(int id)
        {
            Image img = _context.Image.SingleOrDefault(m => m.Id == id);
            string imageBase64Data =Convert.ToBase64String(img.ImageData);
            string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
            return Json(imageDataURL);
        }
    }
}
