﻿using eCommerce.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ColmanNews.Data
{
    public static class DatabaseSeed
    {
        public static void Seed(ColmanNewsContext context)
        {
            if (!context.Users.Any())
            {
                initUsers(context);
            }
            if (!context.Genres.Any())
            {
                initGenres(context);
            }
            if (!context.UserGenres.Any())
            {
                initUserGenres(context);
            }
            if (!context.City.Any())
            {
                initCities(context);
            }
            if (!context.Articles.Any())
            {
                initArticles(context);
            }
        }

        private static void initUsers(ColmanNewsContext context)
        {
            // Readers
            for (int i = 0; i < 5; i++)
            {
                User reader = new User()
                {
                    Username = "reader" + i.ToString(),
                    Password = i.ToString(),
                    UserRole = Role.Reader,
                    BirthDate = new DateTime(),
                    FirstName = "my-firstname-is-" + i,
                    LastName = "my-lastname-is-" + i
                };

                context.Add(reader);
            }

            // Reporters
            var Reporters = new List<User>
            {
                new User
                {
                    Username = "smurfette",
                    Password = "1",
                    UserRole = Role.Reporter,
                    BirthDate = new DateTime(),
                    FirstName = "smurfette",
                    LastName = "Smurf",
                    Address = "Holon, israel "
                },
                new User
                {
                    Username = "paps",
                    Password = "2",
                    UserRole = Role.Reporter,
                    BirthDate = new DateTime(),
                    FirstName = "Papa",
                    LastName = "Smurf",
                    Address = "Raanana, israel "
                },
                new User
                {
                    Username = "brainy",
                    Password = "3",
                    UserRole = Role.Reporter,
                    BirthDate = new DateTime(),
                    FirstName = "Brainy",
                    LastName = "Smurf",
                    Address = "Ramat Gan, israel "
                },
            };

            Reporters.ForEach(r => context.Users.Add(r));

            // Admin
            User admin = new User()
            {
                Username = "admin",
                Password = "123",
                UserRole = Role.Admin,
                BirthDate = new DateTime(),
                FirstName = "Avi",
                LastName = "Simon"
            };

            context.Add(admin);

            context.SaveChanges();
        }

        private static void initGenres(ColmanNewsContext context)
        {
            Genre sports = new Genre()
            {
                Name = "Sports"
            };
            Genre politics = new Genre()
            {
                Name = "Politics"
            };
            Genre health = new Genre()
            {
                Name = "Health"
            };
            Genre economy = new Genre()
            {
                Name = "Economy"
            };
            Genre other = new Genre()
            {
                Name = "Other"
            };

            context.Add(other);
            context.Add(economy);
            context.Add(health);
            context.Add(politics);
            context.Add(sports);
            context.SaveChanges();
        }

        private static void initUserGenres(ColmanNewsContext context)
        {
            var users = context.Users.ToList();
            var genres = context.Genres.ToList();
            foreach (var user in users)
            {
                foreach (var genre in genres)
                {
                    UserGenre userGenre = new UserGenre()
                    {
                        Username = user.Username,
                        User = user,
                        GenreId = genre.Id,
                        Genre = genre
                    };

                    context.Add(userGenre);
                }
            }
            context.SaveChanges();
        }

        private static void initCities(ColmanNewsContext context)
        {
            var Cities = new List<City>
            {
                new City
                {
                    RealCity="Eilat",
                    SmurfsCity="Gargamel's Castle"
                },
                new City
                {
                    RealCity="Tel Aviv",
                    SmurfsCity="Smurf Village"
                },
                new City
                {
                    RealCity="Haifa",
                    SmurfsCity="Forbidden Falls"
                },
                new City
                {
                    RealCity="Jerusalem",
                    SmurfsCity="New York City"
                },
            };

            Cities.ForEach(r => context.City.Add(r));

            context.SaveChanges();
        }

        private static void initArticles(ColmanNewsContext context)
        {
            var genres = context.Genres.ToList();
            var reporters = context.Users.Where(user => user.UserRole == Role.Reporter).ToList();

            var articles = new List<Article> {
                new Article {
                    Title = "Something about sports",
                    Author = reporters[1],
                    AuthorUserName = reporters[1].Username,
                    Comments = new List<Comment>(),
                    Content = "Im writing something about sports so I could have enough articles...",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Sports"),
                    GenreId = genres.Find(gen => gen.Name == "Sports").Id
                },
                new Article {
                    Title = "Macabi Dardasim won the Champions League 2020",
                    Author = reporters[0],
                    AuthorUserName = reporters[0].Username,
                    Comments = new List<Comment>(),
                    Content = "Macabi Dardasim won the Champions League 2020. The striker, Dardasaba, commented: Pure luck.",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Sports"),
                    GenreId = genres.Find(gen => gen.Name == "Sports").Id
                },
                new Article {
                    Title = "Gargamel got Corona :D",
                    Author = reporters[1],
                    AuthorUserName = reporters[1].Username,
                    Comments = new List<Comment>(),
                    Content = "Gargamel got corona. Lucky :)",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Health"),
                    GenreId = genres.Find(gen => gen.Name == "Health").Id
                },
                new Article {
                    Title = "A generic health article",
                    Author = reporters[1],
                    AuthorUserName = reporters[1].Username,
                    Comments = new List<Comment>(),
                    Content = "Its just a random article. Don't notice me",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Health"),
                    GenreId = genres.Find(gen => gen.Name == "Health").Id
                },
                new Article {
                    Title = "A cure for the Smurfvid-20 has been invented",
                    Author = reporters.First(),
                    AuthorUserName = reporters.First().Username,
                    Comments = new List<Comment>(),
                    Content = "Brainsmurf has invented a possible cure to the Smurfvid-20 pandemic. Maybe",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Health"),
                    GenreId = genres.Find(gen => gen.Name == "Health").Id
                },
                new Article { 
                    Title = "Papa smurf got COVID-19",
                    Author = reporters[1],
                    AuthorUserName = reporters[1].Username,
                    Comments = new List<Comment>(),
                    Content = "Papa smurf went to The Smurfboys concert. It was later revealed that the singer had COVID- 19. Then Papa Smurf got checked, and turned out positive.",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Health"),
                    GenreId = genres.Find(gen => gen.Name == "Health").Id
                },
                new Article {
                    Title = "Gargamel's new born son",
                    Author = reporters.First(),
                    AuthorUserName = reporters.First().Username,
                    Comments = new List<Comment>(),
                    Content = "Gargamel has a new son named Benjamin. Now the question is who is the mother???",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Politics"),
                    GenreId = genres.Find(gen => gen.Name == "Politics").Id
                },
                new Article {
                    Title = "Papasmurf has defeated John Snow and took the Iron Throne",
                    Author = reporters[2],
                    AuthorUserName = reporters[2].Username,
                    Comments = new List<Comment>(),
                    Content = "Papasmurf has defeated John Snow and took the Iron Throne. Getting hype for season 9!",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Politics"),
                    GenreId = genres.Find(gen => gen.Name == "Politics").Id
                },
                new Article {
                    Title = "No money left",
                    Author = reporters[2],
                    AuthorUserName = reporters[2].Username,
                    Comments = new List<Comment>(),
                    Content = "No money left because Thievemurf took all the tax money. What we can do now?",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Economy"),
                    GenreId = genres.Find(gen => gen.Name == "Economy").Id
                },
                new Article {
                    Title = "Gransmurf won the lotto",
                    Author = reporters[0],
                    AuthorUserName = reporters[0].Username,
                    Comments = new List<Comment>(),
                    Content = "Gransmurf won the lotto and inveseted it in the government",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Economy"),
                    GenreId = genres.Find(gen => gen.Name == "Economy").Id
                },
                new Article {
                    Title = "Incoming rain",
                    Author = reporters[0],
                    AuthorUserName = reporters[0].Username,
                    Comments = new List<Comment>(),
                    Content = "It will be raining for all next week, sadly :(",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Other"),
                    GenreId = genres.Find(gen => gen.Name == "Other").Id
                },
                new Article {
                    Title = "Smurp lost the elections",
                    Author = reporters[0],
                    AuthorUserName = reporters[0].Username,
                    Comments = new List<Comment>(),
                    Content = "Joe Smurfiden won the elections, and will be the new president of the Smurf Village",
                    CreationDate = new DateTime(),
                    Genre = genres.Find(gen => gen.Name == "Other"),
                    GenreId = genres.Find(gen => gen.Name == "Other").Id
                },
            };

            articles.ForEach(art => context.Articles.Add(art));

            context.SaveChanges();
        }
    }
}
